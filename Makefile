destinationDir=/opt/toggle-check

all: install

install: 
	@sudo mkdir -p $(destinationDir) ||:
	@sudo cp -r . $(destinationDir) ||:
	@mkdir -p ~/.local/share/file-manager/actions  ||:
	@cp f536ec4a-ea85-4ba4-a125-ed62a7a420c7.desktop ~/.local/share/file-manager/actions  ||:		

uninstall:
	@sudo rm $(destinationDir) -rf ||:
	@sudo rm ~/.local/share/file-manager/actions/f536ec4a-ea85-4ba4-a125-ed62a7a420c7.desktop ||: