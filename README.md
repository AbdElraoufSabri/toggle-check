# Toggle Check

adds/remove ✔ to/from files/folders names

## How it Works

<div align="center">
  <a href="https://www.youtube.com/watch?v=3YnBDazvZAk"><img src="https://img.youtube.com/vi/3YnBDazvZAk/0.jpg" alt="Click to watch"></a>
</div>

## Installation
1. clone this repository `git clone https://gitlab.com/AbdElraoufSabri/toggle-check.git`
1. `cd toggle-check`
1. `sudo make install`

## Uninstall 
1. `cd /opt/toggle-check`
1. `sudo make uninstall`

that's it

## Tested on
- [x] Ubuntu 18.04
- [ ] Ubuntu 16.04
- [ ] Ubuntu 14.04